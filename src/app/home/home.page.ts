import { Component } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  

  browserview = false;
  options: InAppBrowserOptions ={
    location : 'no',//'yes' Or 'no' 
    hidden : 'no', //Or  'yes'
    hideurlbar: 'no',

  };

  constructor(private browser : InAppBrowser) {
    
    this.openUrl();
    
    
  }
//segunda version


  openUrl(){
    
    this.browser.create('https://www.stella.proyectosfit.cl/productos/', '_self', this.options)

  }

}
